# Seattle Public Schools (SPS) Alexa Calendar Skill

# Build
```bash
# Get dependencies (one-time)
go get github.com/aws/aws-lambda-go/lambda

# Build
go build sps-cal.go

```

# Deploy

# References
* [Alexa Skill](https://developer.amazon.com/alexa-skills-kit)
* [Alexa SDK for GoLang](https://aws.amazon.com/sdk-for-go/)
* [Seattle Public Schools Calendars](https://www.seattleschools.org/district/calendars/)
