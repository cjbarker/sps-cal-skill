package main

import (
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
)

func Handler() (string, error) {
	return fmt.Sprintf("hello world"), nil
}

func main() {
	lambda.Start(Handler)
}
